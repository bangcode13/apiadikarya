<?php

namespace App\Transformers;

use App\Post;
use League\Fractal\TransformerAbstract;

class PostTransformer extends TransformerAbstract{

	protected $defaultIncludes=[
		'user'
	];

	public function transform(Post $post){
		return [
			'post_id'=>(int) $post->id,
			'title'=>$post->title,
			'content'=>$post->content,
			'category'=>$post->category,
			'image'=>$post->image,
			'created_at'=>date('d F Y',strtotime($post->created_at)),
			'updated_at'=>date('d F Y',strtotime($post->updated_at)),
			'user'=>$post->user
		];
	}


	public function includeUser(Post $post){
		$author=$post->user->first();

		return $this->item($author, new UserTransformer);
	}
}