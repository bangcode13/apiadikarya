<?php

namespace App\Transformers;

use App\User;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract{


	public function transform(User $user){
		return [
			'user_id'=>(int) $user->id,
			'name'=>$user->name,
			'username'=>$user->username,
			'email'=>$user->email
			
		];
	}
}