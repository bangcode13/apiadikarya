<?php

use Illuminate\Database\Seeder;
use App\Permission;



class PermissionTableSeeder extends Seeder
{
	public function run()
	{
		$createUser=new Permission();
		$createUser->name='create-user';
		$createUser->display_name='Create User';
		$createUser->description='create new user';
		$createUser->save();

		$editUser=new Permission();
		$editUser->name='edit-user';
		$editUser->display_name='Edit User';
		$editUser->description='edit user';
		$editUser->save();

		$deleteUser=new Permission();
		$deleteUser->name='delete-user';
		$deleteUser->display_name='delete User';
		$deleteUser->description='delete user';
		$deleteUser->save();

		$createShopDrawing=new Permission();
		$createShopDrawing->name='create-shopDrawing';
		$createShopDrawing->display_name='Create ShopDrawing';
		$createShopDrawing->description='create new ShopDrawing';
		$createShopDrawing->save();

		$editShopDrawing=new Permission();
		$editShopDrawing->name='edit-shopDrawing';
		$editShopDrawing->display_name='Edit ShopDrawing';
		$editShopDrawing->description='edit ShopDrawing';
		$editShopDrawing->save();

		$deleteShopDrawing=new Permission();
		$deleteShopDrawing->name='delete-shopDrawing';
		$deleteShopDrawing->display_name='Delete ShopDrawing';
		$deleteShopDrawing->description='delete ShopDrawing';
		$deleteShopDrawing->save();

		$createProgress=new Permission();
		$createProgress->name='create-progress';
		$createProgress->display_name='Create Progress';
		$createProgress->description='create new progress';
		$createProgress->save();

		$editProgress=new Permission();
		$editProgress->name='edit-progress';
		$editProgress->display_name='Edit Progress';
		$editProgress->description='edit Progress';
		$editProgress->save();

		$deleteProgress=new Permission();
		$deleteProgress->name='delete-progress';
		$deleteProgress->display_name='Delete Progress';
		$deleteProgress->description='delete Progress';
		$deleteProgress->save();

		$createProcurement=new Permission();
		$createProcurement->name='create-procurement';
		$createProcurement->display_name='Create Procurement';
		$createProcurement->description='create new procurement';
		$createProcurement->save();

		$editProcurement=new Permission();
		$editProcurement->name='edit-procurement';
		$editProcurement->display_name='Edit procurement';
		$editProcurement->description='edit procurement';
		$editProcurement->save();

		$deleteProcurement=new Permission();
		$deleteProcurement->name='delete-procurement';
		$deleteProcurement->display_name='Delete procurement';
		$deleteProcurement->description='delete procurement';
		$deleteProcurement->save();

		$createqc=new Permission();
		$createqc->name='create-qc';
		$createqc->display_name='Create qc';
		$createqc->description='create new qc';
		$createqc->save();

		$editqc=new Permission();
		$editqc->name='edit-qc';
		$editqc->display_name='Edit qc';
		$editqc->description='edit qc';
		$editqc->save();

		$deleteqc=new Permission();
		$deleteqc->name='delete-qc';
		$deleteqc->display_name='Delete qc';
		$deleteqc->description='delete qc';
		$deleteqc->save();

		$createk3l=new Permission();
		$createk3l->name='create-k3l';
		$createk3l->display_name='Create k3l';
		$createk3l->description='create new k3l';
		$createk3l->save();

		$editk3l=new Permission();
		$editk3l->name='edit-k3l';
		$editk3l->display_name='Edit k3l';
		$editk3l->description='edit k3l';
		$editk3l->save();

		$deletek3l=new Permission();
		$deletek3l->name='delete-k3l';
		$deletek3l->display_name='Delete k3l';
		$deletek3l->description='delete k3l';
		$deletek3l->save();

		$createKeuangan=new Permission();
		$createKeuangan->name='create-keuangan';
		$createKeuangan->display_name='Create keuangan';
		$createKeuangan->description='create new keuangan';
		$createKeuangan->save();

		$editKeuangan=new Permission();
		$editKeuangan->name='edit-keuangan';
		$editKeuangan->display_name='Edit keuangan';
		$editKeuangan->description='edit keuangan';
		$editKeuangan->save();

		$deleteKeuangan=new Permission();
		$deleteKeuangan->name='delete-keuangan';
		$deleteKeuangan->display_name='Delete keuangan';
		$deleteKeuangan->description='delete keuangan';
		$deleteKeuangan->save();
	}
}
