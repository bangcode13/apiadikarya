<?php

use Illuminate\Database\Seeder;
use App\Post;
use App\User;


class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {	
    	
    	

        Post::create([
          'title'=>'Keuangan negara',
          'content'=>'lorem ipsum dolor sit amet',
          'category'=>'keuangan',
          'image'=>'hahaha',
          'user_id'=>3
          ]);

        Post::create([
          'title'=>'Progress Terbaru',
          'content'=>'lorem ipsum dolor sit amet',
          'category'=>'progress',
          'image'=>'hehehe',
          'user_id'=>3
          ]);

        // Post::create([
        //   'title'=>'dolor Ipsum',
        //   'content'=>'dolor dolor dolor dolor satu',
        //   'category'=>'QC',
        //   'image'=>'dolor',
        //   'user_id'=>4
        //   ]);
    }
}
