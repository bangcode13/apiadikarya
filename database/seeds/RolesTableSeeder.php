<?php

use Illuminate\Database\Seeder;
use App\Role;



class RolesTableSeeder extends Seeder
{
    public function run()
    {
        
        $owner= new Role();
        $owner->name = 'manager';
        $owner->display_name='Project manager';
        $owner->description='User Is the people who has manage project';
        $owner->save();

        $admin= new Role();
        $admin->name = 'admin';
        $admin->display_name='Admin User';
        $admin->description='Admin User is people who has manage user';
        $admin->save();

        $shopdrawing= new Role();
        $shopdrawing->name = 'shopdrawing';
        $shopdrawing->display_name='Shop Drawing Division';
        $shopdrawing->description='Shop Drawing Division';
        $shopdrawing->save();

        $progress= new Role();
        $progress->name = 'progress';
        $progress->display_name='Progress Division';
        $progress->description='Progress Division';
        $progress->save();

        $procurement= new Role();
        $procurement->name = 'procurement';
        $procurement->display_name='Procurement Division';
        $procurement->description='Procurement Division';
        $procurement->save();

        $qc= new Role();
        $qc->name = 'qc';
        $qc->display_name='QC Division';
        $qc->description='QC Division';
        $qc->save();

        $k3l= new Role();
        $k3l->name = 'k3l';
        $k3l->display_name='K3L Division';
        $k3l->description='K3L Division';
        $k3l->save();

        $keuangan= new Role();
        $keuangan->name = 'keuangan';
        $keuangan->display_name='Keuangan Division';
        $keuangan->description='Keuangan Division';
        $keuangan->save();
    }
}
