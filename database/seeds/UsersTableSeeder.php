<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use App\User;

class UsersTableSeeder extends Seeder
{
    public function run()
    {
        User::create([
        		'name'=>'admin',
        		'username'=>'admadi',
        		'email'=>'admin@gmail.com',
        		'password'=>Hash::make('1234')
        	]);

         // User::create([
        	// 	'name'=>'manager',
        	// 	'username'=>'mngadi',
        	// 	'email'=>'manager@gmail.com',
        	// 	'password'=>Hash::make('1234')
        	// ]);
    }
}
